const express = require("express");
const controllers = require("../app/controllers");
const YAML = require("yamljs");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = YAML.load("./openapi.yaml");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */

// Cars
apiRouter.get(
  "/api/v1/cars",
  controllers.api.v1.authController.authorize(1, 2, 3),
  controllers.api.v1.carController.list
);
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.authController.authorize(1, 2, 3),
  controllers.api.v1.carController.show
);
apiRouter.get(
  "/api/v1/deletedcars",
  controllers.api.v1.authController.authorize(1),
  controllers.api.v1.carController.showDeleted
);
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.authController.authorize(1, 2),
  controllers.api.v1.carController.create
);
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.authController.authorize(1, 2),
  controllers.api.v1.carController.update
);
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.authController.authorize(1, 2),
  controllers.api.v1.carController.destroy
);

// login users
apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);
// Member
apiRouter.post(
  "/api/v1/member/add",
  controllers.api.v1.userController.createMember
);
// admin
apiRouter.post(
  "/api/v1/admin/add",
  controllers.api.v1.authController.authorize(1),
  controllers.api.v1.userController.createAdmin
);
// check current user
apiRouter.get(
  "/api/v1/whoami",
  controllers.api.v1.authController.authorize(1, 2, 3),
  controllers.api.v1.authController.whoAmI
);

// add type user
apiRouter.post(
  "/api/v1/typeuser",
  controllers.api.v1.authController.authorize(1),
  controllers.api.v1.userController.createType
);

// open Api
apiRouter.get("/api/v1/docs/swagger.json", (req, res) => {
  res.status(200).json(swaggerDocument);
});
apiRouter.use(
  "/api/v1/docs",
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument)
);

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
