const carRepository = require("../repositories/carRepository");
const { operations } = require("../models");

module.exports = {
  async list() {
    try {
      const cars = await carRepository.findAll({
        include: [{ model: operations }],
      });
      const totalCar = await carRepository.getTotalCar();

      return {
        data: cars,
        count: totalCar,
      };
    } catch (err) {
      throw err;
    }
  },

  async listAllDeleted(showDeleted = true) {
    try {
      const cars = await carRepository.findAllDeleted(showDeleted);

      return {
        data: cars,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carRepository.find(id);
  },

  create(requestBody) {
    return carRepository.create(requestBody);
  },

  update(id, requestBody) {
    return carRepository.update(id, requestBody);
  },

  delete(id) {
    return carRepository.delete(id);
  },
};
