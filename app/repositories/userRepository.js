const { typeusers, users } = require('../models')

module.exports = {
  // create type user -> temporary
  createType(createArgs) {
    return typeusers.create(createArgs)
  },

  update(id, updateArgs) {
    return users.update(updateArgs, {
      where: {
        id,
      },
    })
  },

  delete(id) {
    return users.destroy(id)
  },

  find(email) {
    return users.findOne(email)
  },

  findAll() {
    return users.findAll()
  },

  findId(id) {
    return users.findByPk(id)
  },

  getByEmail(email) {
    return users.findOne({
      where: {
        email,
      },
    })
  },

  create(user) {
    return users.create({
      email: user.email,
      password: user.password,
      idtype: user.idtype,
      createdAt: new Date(),
      updatedAt: new Date(),
    })
  },
}
