const userService = require('../../../services/userService')
const bcrypt = require('bcrypt')

// create member
const createMember = async (req, res) => {
  const existedUser = await userService.findByEmail(req.body.email)
  if (existedUser) {
    return res.status(400).send({
      message: 'Email is used before',
    })
  }
  const hashedPassword = await bcrypt.hash(req.body.password, 10)
  const user = {
    email: req.body.email,
    password: hashedPassword,
    idtype: 3,
    createdAt: new Date(),
    updatedAt: new Date(),
  }
  try {
    const users = await userService.create(user)
    res.status(201).json({
      message: 'member Created',
      data: [{email:users.email, idtype: users.idtype,createdAt: users.createdAt,updatedAt:users.updatedAt}],
    })
  } catch (error) {
    res.status(400).send(error)
  }
}

// create Admin
const createAdmin = async (req, res) => {
  const existedUser = await userService.findByEmail(req.body.email)
  if (existedUser) {
    return res.status(400).send({
      message: 'Email is used before',
    })
  }
  const hashedPassword = await bcrypt.hash(req.body.password, 10)
  const user = {
    email: req.body.email,
    password: hashedPassword,
    idtype: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
  }
  try {
    const users = await userService.create(user)
    res.status(201).json({
      message: 'admin Created',
      data: [{email:users.email, idtype: users.idtype,createdAt: users.createdAt,updatedAt:users.updatedAt}],
    })
  } catch (error) {
    res.status(400).send(error)
  }
}

module.exports = {
  // temporary insert typeuser
  async createType(req, res) {
    try {
      const typeUser = await userService.createType(req.body)
      res.status(200).json(typeUser)
    } catch (error) {
      res.status(422).json({
        status: 'FAIL',
        message: 'Role failed Created',
      })
    }
  },
  createMember,
  createAdmin,
}
