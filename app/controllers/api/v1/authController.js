const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const userService = require('../../../services/userService')

// function create token
function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || 'secret', {
    expiresIn: 10 * 60,
  })
}

module.exports = {
  // function login
  async login(req, res) {
    const email = req.body.email.toLowerCase()
    const hashedPassword = await bcrypt.hash(req.body.password, 10)

    // check email exits in database or not
    const user = await userService.get({ where: { email } })
    if (!user) {
      res.status(404).json({ message: 'Email not found' })
      return
    }

    // check password
    const check = bcrypt.compare(hashedPassword, user.password, function (
      err,
      result,
    ) {
      if (err) {
        throw err
      }
      return result
    })

    if (check === false) {
      res.status(401).json({
        message: 'Password wrong!',
      })
      return
    }

    // create token
    const token = createToken({
      id: user.id,
      email: user.email,
      role: user.idtype,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    })

    res.status(201).json({
      id: user.id,
      email: user.email,
      token,
      role: user.idtype,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    })
  },

  // authorize user
  authorize: (...roles) => async (req, res, next) => {
    try {
      const bearerToken = req.headers.authorization
      const token = bearerToken.split('Bearer ')[1]
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || 'secret',
      )

      req.user = await userService.find(tokenPayload.id)

      if (roles.length > 0) {
        if (!roles.includes(req.user.idtype)) {
          res.status(401).json({
            message: 'Anda tidak punya akses (Unauthorized)',
          })
          return
        }
      }

      next()
    } catch (error) {
      if (error.message.includes('jwt expired')) {
        res.status(401).json({ message: 'Token Expired' })
        return
      }

      res.status(401).json({
        message: 'Unauthorized',
      })
    }
  },

  // check current user
  async whoAmI(req, res) {
    let role = ''
    if (req.user.idtype === 1) {
      role = 'Superadmin'
    } else if (req.user.idtype === 2) {
      role = 'Admin'
    } else if (req.user.idtype === 3) {
      role = 'Member'
    }
    res.status(201).json({
      data: { id: req.user.id, email: req.user.email },
      desc: `Anda adalah ${role}`,
    })
  },
}
