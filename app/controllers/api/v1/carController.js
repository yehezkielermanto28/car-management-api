const carService = require("../../../services/carService");
const userService = require("../../../services/userService");
const operationsService = require("../../../services/operationsService");
const { operations } = require("../../../models");

module.exports = {
  async list(req, res) {
    try {
      const cars = await carService.list({
        include: [{ model: operations }],
      });
      if (cars.length === 0) {
        res
          .status(404)
          .json({ message: "Cars are empty, please insert the data" });
        return;
      }
      res.status(200).json({
        status: "OK",
        cars,
        operations,
      });
    } catch (err) {
      res.status(400).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },

  async show(req, res) {
    try {
      const car = await carService.get(req.params.id);
      if (!car) {
        res.status(404).json({
          status: "FAIL",
          message: "Car not found",
        });
        return;
      }
      const user = await userService.find(car.operations[0].iduser);
      const userInfo = {
        id: user.id,
        email: user.email,
      };
      const data = {
        car,
        userInfo,
      };
      res.status(200).json({
        status: "OK",
        data: data,
      });
    } catch (err) {
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },

  async showDeleted(req, res) {
    try {
      const cars = await carService.listAllDeleted();
      if (cars.length === 0) {
        res
          .status(404)
          .json({ message: "Cars are empty, please insert the data" });
        return;
      }
      res.status(200).json({
        status: "OK",
        data: cars,
      });
    } catch (err) {
      res.status(400).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },

  async create(req, res) {
    try {
      if (req.body.name == null || req.body.capacity == null) {
        res.status(400).json({
          status: "FAIL",
          message: "Name and Capacity are required",
        });
        return;
      }
      // create car
      const car = await carService.create({
        name: req.body.name,
        capacity: req.body.capacity,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      const operations = await operationsService.create({
        idcar: car.id,
        iduser: req.user.id,
        createdBy: req.user.id,
        updatedBy: null,
        deletedBy: null,
      });
      const user = await userService.find(operations.iduser);
      const userInfo = {
        id: user.id,
        email: user.email,
      };
      const data = {
        car,
        operations,
        userInfo,
      };
      res.status(201).json({
        status: "Car has been created successfully",
        data: data,
      });
    } catch (err) {
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },

  async update(req, res) {
    try {
      if (req.body.name == null || req.body.capacity == null) {
        res.status(400).json({
          status: "FAIL",
          message: "Name and Capacity are required",
        });
        return;
      }
      const car = await carService.get(req.params.id);
      if (!car) {
        res.status(404).json({
          status: "FAIL",
          message: "Car not found",
        });
        return;
      }

      await carService.update(req.params.id, req.body);
      await operationsService.update(req.params.id, {
        updatedBy: req.user.id,
        updatedAt: new Date(),
      });
      console.log(req.params.id);
      console.log(req.user.id);
      // get user info
      const user = await userService.find(req.user.id);
      const userInfo = {
        id: user.id,
        email: user.email,
      };
      // get car info
      const carUpdated = await carService.get(req.params.id);
      const data = {
        car: carUpdated,
        userInfo,
      };
      // get operations info
      res.status(200).json({
        status: "OK",
        message: "Car has been updated successfully",
        data: data,
      });
    } catch (err) {
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },

  async destroy(req, res) {
    try {
      const car = await carService.get(req.params.id);
      if (!car) {
        res.status(404).json({
          status: "FAIL",
          message: "Car not found",
        });
        return;
      }
      await carService.delete(req.params.id);
      await operationsService.update(req.params.id, {
        deletedBy: req.user.id,
        updatedAt: new Date(),
      });
      res.status(200).json({
        status: "OK",
        message: "Car has been deleted successfully",
      });
    } catch (err) {
      res.status(422).json({
        status: "FAIL",
        message: err.message,
      });
    }
  },
};
